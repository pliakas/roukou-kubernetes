#!/bin/bash

echo "Setting Java Environment"
export JAVA_HOME=~/.tools/dev/jdk/open-jdk-11/
export PATH=$JAVA_HOME/bin:$PATH

echo "Step 5: deploying ingress"
kubectl delete service/organization deployment.apps/organization
kubectl delete service/department deployment.apps/department
kubectl delete service/employee deployment.apps/employee
kubectl delete service/gateway deployment.apps/gateway
