package org.roukou.kubernetes.services.organization.repository;

import org.roukou.kubernetes.services.organization.model.Organization;
import org.springframework.data.repository.CrudRepository;

public interface OrganizationRepository extends CrudRepository<Organization, String> {

}
