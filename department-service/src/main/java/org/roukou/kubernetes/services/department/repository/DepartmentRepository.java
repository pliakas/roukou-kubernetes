package org.roukou.kubernetes.services.department.repository;

import java.util.List;
import org.roukou.kubernetes.services.department.model.Department;
import org.springframework.data.repository.CrudRepository;

public interface DepartmentRepository extends CrudRepository<Department, String> {

    List<Department> findByOrganizationId(Long organizationId);

}
