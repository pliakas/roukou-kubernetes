#!/bin/bash

echo "$(date +'%b-%d-%y, %H:%M:%S') - INFO: Setting Java Environment"
export JAVA_HOME=~/.tools/dev/jdk/open-jdk-11/
export PATH=$JAVA_HOME/bin:$PATH

echo "$(date +'%b-%d-%y, %H:%M:%S') - INFO:  Buid Microservices"
mvn clean install

echo "Build Containers Images"
cd ./employee-service && docker build -t localhost:32000/roukou/employee:1.0 . && docker push localhost:32000/roukou/employee:1.0 && cd ..
cd ./organization-service && docker build -t localhost:32000/roukou/organization:1.0 . &&  docker push localhost:32000/roukou/organization:1.0 && cd ..
cd ./department-service && docker build -t localhost:32000/roukou/department:1.0 . && docker push localhost:32000/roukou/department:1.0 && cd ..
cd ./gateway-service && docker build -t localhost:32000/roukou/gateway:1.0 . && docker push localhost:32000/roukou/gateway:1.0 && cd ..

#echo "$(date +'%b-%d-%y, %H:%M:%S') - INFO: Setting docker registry to be used by minikube"
#eval $(minikube docker-env)

echo "$(date +'%b-%d-%y, %H:%M:%S') - INFO:  Deploying to kubernetes cluster"
echo "$(date +'%b-%d-%y, %H:%M:%S') - INFO: Step 1: Configure mongodb - configmap"
kubectl apply -f kubernetes/mongodb-configmap.yaml
kubectl apply -f kubernetes/mongodb-secret.yaml

echo "$(date +'%b-%d-%y, %H:%M:%S') - INFO:  Step 2: Deploying mongodb"
kubectl apply -f kubernetes/mongodb-deployment.yaml

echo "$(date +'%b-%d-%y, %H:%M:%S') - INFO:  Step 3: deploying employee microserivce"
kubectl apply -f kubernetes/employee-deployment.yaml

echo "$(date +'%b-%d-%y, %H:%M:%S') - INFO:  Step 3: deploying department microserivce"
kubectl apply -f kubernetes/department-deployment.yaml

echo "$(date +'%b-%d-%y, %H:%M:%S') - INFO:  Step 3: deploying organization microserivce"
kubectl apply -f kubernetes/organization-deployment.yaml

echo "$(date +'%b-%d-%y, %H:%M:%S') - INFO: Step 3: deploying gateway microserivce"
kubectl apply -f kubernetes/gateway-deployment.yaml

echo "$(date +'%b-%d-%y, %H:%M:%S') - INFO:  Step 5: deploying ingress"
kubectl apply -f kubernetes/ingress.yaml

