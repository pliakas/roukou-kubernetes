package org.roukou.kubernetes.services.employee.repository;

import java.util.List;
import org.roukou.kubernetes.services.employee.model.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, String> {

    List<Employee> findByDepartmentId(Long departmentId);

    List<Employee> findByOrganizationId(Long organizationId);

}
